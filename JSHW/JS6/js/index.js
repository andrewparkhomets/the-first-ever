"use strict";
function filterBy(array, dataType) {
  if (Array.isArray(array)) {
    const newArray = array.filter((element) => typeof element !== dataType);
    console.log(newArray);
  } else {
    alert("Error");
  }
}

const arr = [
  "Элемент массива",
  "string",
  14,
  64,
  null,
  undefined,
  null,
  function () {},
  [],
];
const filter = prompt(
  "Введите тип данных, которые хотите отфильтровать:",
  "undefined, number, boolean, string, symbol, object, function"
);
filterBy(arr, filter);
