"use strict";
let number = null;
do {
  number = prompt("Enter the number");
} while (number === "" || Number.isNaN(+number));
console.log(number, "users number");

let i = 0;
while (i <= +number) {
  if (i % 5 === 0) {
    console.log(i);
  }
  i++;
}
